void main(List<String> args) {
  double setengah, alas, tinggi;
  setengah = 0.5;
  alas = 20.0;
  tinggi = 30.0;

  var luasSegitiga = setengah * alas * tinggi;
  print(luasSegitiga);
}

class Segitiga {
  double setengah;
  double alas;
  double tinggi;

  double hitungLuas() {
    return this.setengah * this.alas * this.tinggi;
  }
}
