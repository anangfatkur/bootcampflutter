import 'package:flutter/material.dart';
// import 'package:sanberappflutter2/Tugas/Tugas13/HomeScreen.dart';
// import 'package:sanberappflutter2/Tugas/Tugas13/HomeScreen.dart';
// import 'package:sanberappflutter2/Tugas/Tugas14/Get_data2.dart';
// import 'Quiz3/MainApp.dart';
// import 'Tugas/Tugas12/Telegram.dart';
// import 'Tugas/Tugas13/HomeScreen.dart';
// import 'Tugas/Tugas13/LoginScreen.dart';
import 'Quiz3/LoginScreen.dart';
// import 'Quiz3/HomeScreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
      // home: MainApp(),
    );
  }
}

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MainApp(),
//     );
//   }
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MainApp(),
//     );
//   }
// }

// class MyApp extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Flutter Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       // home: GetDataApi(),
//       home: GetData2(),
//     );
//   }
// }

// /// TUGAS 15 ////

// // import 'package:flutter/material.dart';
// // import 'package:sanberappmobile/Tugas/Tugas15/AccountScreen.dart';
// // import 'package:sanberappmobile/Tugas/Tugas15/SearchScreen.dart';
// // import 'HomeScreen.dart';

// // class MainScreen extends StatefulWidget {
// //   @override
// //   _MainScreenState createState() => _MainScreenState();
// // }

// // class _MainScreenState extends State<MainScreen> {
// //   int _selectedIndex = 0;
// //   final _layoutPage =[
// //     HomeScreen(),
// //     SearchScreen(),
// //     AccountScreen(),
// //   ];
// //   void _onTabItem(int index){
// //     setState((){
// //       _selectedIndex = index;
// //     });
// //   }
// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       body: _layoutPage.elementAt(_selectedIndex),
// //       bottomNavigationBar: BottomNavigationBar(
// //         items: <BottomNavigationBarItem>[
// //           BottomNavigationBarItem(
// //             icon: Icon(Icons.home),
// //             title: Text("Home")
// //           ),
// //            BottomNavigationBarItem(
// //             icon: Icon(Icons.search),
// //             title: Text("Search")
// //           ),
// //            BottomNavigationBarItem(
// //             icon: Icon(Icons.account_circle_sharp),
// //             title: Text("Account")
// //           ),
// //         ],
// //         type: BottomNavigationBarType.fixed,
// //         currentIndex: _selectedIndex,
// //         onTap: _onTabItem,
// //       ),
// //     );
// //   }
// // }
