// Soal No.1 (Range) //

// void main(List<String> args) {
//   print(range(11, 18));
// }

// range(
//   startNum,
//   finishNum,
// ) {
//   var list = [for (var i = startNum; i <= finishNum; i += 1) i];
//   print(list);
// }

// Soal No.2 (Range With Step) //

// void main(List<String> args) {
//   print(range(11, 23, 3));
// }

// range(startNum, finishNum, step) {
//   var list = [for (var i = startNum; i <= finishNum; i += step) i];
//   print(list);
// }

// Soal No.3 (List Multidime) //

// void main(List<String> args) {
//   print(dataHandling());
// }

// dataHandling() {
//   var input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
//   ];
//   for (var i = 0; i < input[3].length; i++) {
//     print("Nomor ID : " + input[i][0].toString());
//     print("Nama Lengkap : " + input[i][1].toString());
//     print("TTL : " + input[i][2].toString() + ' ' + input[i][3].toString());
//     print("Hobi : " + input[i][4].toString());
//     print("");
//   }
// }

// void main(List<String> args) {
//   print(dataHandling());
// }

// dataHandling() {
//   var input = [
//     ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
//     ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
//     ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
//     ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
//   ];
//   print("Nomor ID :" +
//       " " +
//       input[0][0] +
//       "\nNama Lengkap: " +
//       " " +
//       input[0][1] +
//       "\nTTL :" +
//       " " +
//       input[0][2] +
//       " " +
//       input[0][3] +
//       " " +
//       "\nHobi:" +
//       " " +
//       input[0][4] +
//       "\n");
//   print("Nomor ID :" +
//       " " +
//       input[1][0] +
//       "\nNama Lengkap: " +
//       " " +
//       input[1][1] +
//       "\nTTL :" +
//       " " +
//       input[1][2] +
//       " " +
//       input[1][3] +
//       " " +
//       "\nHobi:" +
//       " " +
//       input[1][4] +
//       "\n");
//   print("Nomor ID :" +
//       " " +
//       input[2][0] +
//       "\nNama Lengkap: " +
//       " " +
//       input[2][1] +
//       "\nTTL :" +
//       " " +
//       input[2][2] +
//       " " +
//       input[2][3] +
//       " " +
//       "\nHobi:" +
//       " " +
//       input[2][4] +
//       "\n");
//   print("Nomor ID :" +
//       " " +
//       input[3][0] +
//       "\nNama Lengkap: " +
//       " " +
//       input[3][1] +
//       "\nTTL :" +
//       " " +
//       input[3][2] +
//       " " +
//       input[3][3] +
//       " " +
//       "\nHobi:" +
//       " " +
//       input[3][4]);
// }

// Soal No.4 (Balik Kata) //

void main(List<String> args) {
  print(balikKata("Kasur"));
  print(balikKata("SanberCode"));
  print(balikKata("Haji"));
  print(balikKata("racecar"));
  print(balikKata("Sanbers"));
}

balikKata(kata) {
  print(kata.split('').reversed.join(''));
}
